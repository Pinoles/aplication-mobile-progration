import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Text, View, Button,TextInput } from 'react-native';

export default function OfuscarComponent() {
  const [inputText, onChangeiText] = React.useState("");
  const [text, setText] = React.useState("")
  
  const onPressOfusque = function(){
    let texttoofuscar = inputText.split("");
    let textofuscado = "";
    for (let index = 0; index < texttoofuscar.length; index++) {
      const element = texttoofuscar[index];
      textofuscado += element;
      textofuscado += "1";
    }
    setText(textofuscado);
  }
  
  const onPressDesOfusque = function(){
    let texttodifuscar = inputText.split("");
    let textdifuscado = "";
    for (let index = 0; index < texttodifuscar.length; index++) {
      const element = texttodifuscar[index];
      if((index+1)%2 != 0){
        textdifuscado += element;
      }
      
    }
    setText(textdifuscado);
  };

  return (
    <View>
      <Text>{text}</Text>
      <TextInput
        style={{borderWidth: 1}}
        onChangeText={onChangeiText}
        value={inputText}
      />

      <Button
        onPress={onPressOfusque}
        title="ofusque"
        color="#841584"
      />
      <Button
        onPress={onPressDesOfusque}
        title="des-ofusque"
        color="#841584"
      />
      <StatusBar style="auto" />
    </View>
  );
}