import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import { Card, ListItem} from 'react-native-elements';
import { useNavigation } from '@react-navigation/core';

export function Usuarios(){
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/users')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  const renderItem = ({ item }) => (
    <ListItem bottomDivider onPress={() => {navigation.navigate("User",{userId: item.id})}}>
      <ListItem.Content>
        <ListItem.Title>{item.name}</ListItem.Title>
        <ListItem.Subtitle>{item.email}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>
  );

  return (
    <View style={{ flex: 1 }}>
      {isLoading ? <ActivityIndicator/> : (
          <FlatList
          keyExtractor={({ id }, index) => id.toString()}
          data={data}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};

export function Usuario({route}){
  const {userId} = route.params;
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState({});

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/users/'+userId)
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return(
    <View style={{ flex: 1 }}>
        {isLoading ? <ActivityIndicator/> : (
          <Card>
            <Card.Title>{data.name} | {data.username}</Card.Title>
            <Card.Title>{data.email} | {data.phone} </Card.Title>
            <Card.Title>{data.website}</Card.Title>
            <Card.Divider/>
            <View>
              <Text>calle: {data.address.street}</Text>
              <Text>colonia: {data.address.suite}</Text>
              <Text>ciudad: {data.address.city}</Text>
              <Text>codigo postal: {data.address.zipcode}</Text>
              <Text>geo: {data.address.geo.lat} , {data.address.geo.lng}</Text>
            </View>
            <Card.Divider/>
            <View>
              <Text>nombre compañia: {data.company.name}</Text>
              <Text>Frase compañia: {data.company.catchPhrase}</Text>
              <Text>BS compañia: {data.company.bs}</Text>
            </View>
        </Card>
      )}
    </View>
  );
}