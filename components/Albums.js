import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, View } from 'react-native';
import { Card, ListItem, Image} from 'react-native-elements';
import { useNavigation } from '@react-navigation/core';

export function Albums(){
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/albums')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  const renderItem = ({ item }) => (
    <ListItem bottomDivider onPress={() => {navigation.navigate("Photos",{albumId: item.id})}}>
      <ListItem.Content>
        <ListItem.Title>{item.title}</ListItem.Title>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>
  );

  return (
    <View style={{ flex: 1 }}>
      {isLoading ? <ActivityIndicator/> : (
          <FlatList
          keyExtractor={({ id }, index) => id.toString()}
          data={data}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};

export function Photos({route}){
  const {albumId} = route.params;
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/photos?albumId='+albumId)
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  const renderItem = ({ item }) => (
    <Card>
        <Card.Title>{item.title}</Card.Title>
        <Card.Divider/>
        <View style={{flex:1}}>
          <Image
            source={{ uri: item.url }}
            style={{ width: 200, height: 200 }}
          />
        </View>
    </Card>
  );

  return (
    <View style={{ flex: 1 }}>
      {isLoading ? <ActivityIndicator/> : (
          <FlatList
          keyExtractor={({ id }, index) => id.toString()}
          data={data}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};