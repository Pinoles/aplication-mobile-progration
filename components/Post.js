import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import { Card, ListItem} from 'react-native-elements';
import { useNavigation } from '@react-navigation/core';

export function Posts(){
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/posts')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  const renderItem = ({ item }) => (
    <ListItem bottomDivider onPress={() => {navigation.navigate("Comments",{postId: item.id})}}>
      <ListItem.Content>
        <ListItem.Title>{item.title}</ListItem.Title>
        <ListItem.Subtitle>{item.body}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>
  );

  return (
    <View style={{ flex: 1 }}>
      {isLoading ? <ActivityIndicator/> : (
          <FlatList
          keyExtractor={({ id }, index) => id.toString()}
          data={data}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};

export function Comments({route}){
  const {postId} = route.params;
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/comments?postId='+postId)
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  const renderItem = ({ item }) => (
    <Card>
            <Card.Title>{item.name}</Card.Title>
            <Text>
                {item.email}
            </Text>
            <Card.Divider/>
            <Text>
                {item.body}
            </Text>
        </Card>
  );

  return (
    <View style={{ flex: 1 }}>
      {isLoading ? <ActivityIndicator/> : (
          <FlatList
          keyExtractor={({ id }, index) => id.toString()}
          data={data}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};