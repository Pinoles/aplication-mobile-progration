import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
import {Usuarios, Usuario} from './Usuarios';
import {Posts, Comments} from './Post';
import {Albums, Photos} from './Albums';

const Stack = createStackNavigator();

export function UsersStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name="Users" component={Usuarios} options={{title: "Users"}} />
            <Stack.Screen name="User" component={Usuario} options={{title: "User"}}/>
        </Stack.Navigator>
    );
};

export function PostsStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name="Post" component={Posts} options={{title: "Post"}} />
            <Stack.Screen name="Comments" component={Comments} options={{title: "Comments"}} />
        </Stack.Navigator>
    );
};

export function AlbumsStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name="Albums" component={Albums} options={{title: "Albums"}} />
            <Stack.Screen name="Photos" component={Photos} options={{title: "Photos"}} />
        </Stack.Navigator>
    );
};
